from django.shortcuts import render, redirect, reverse
from django.views import generic
from users.forms import AuthForm
from cart.cart import Cart, Order
from products.models import Products
from .forms import MakeOrders
from .models import Orders


class CartPage(generic.View):
    def get(self, request):
        context = {
            'cart': Cart(request),
            'order_forms': MakeOrders(),
            'auth_form': AuthForm(),
        }
        return render(request, 'cart/cart_page.html', context)


class AddToCart(generic.View):
    def post(self, request, pk):
        if request.user.is_authenticated:
            cart = Cart(request, user=request.user)
        else:
            cart = Cart(request)
        product = Products.objects.get(pk=pk)
        cart.add_or_update_item(product)
        return redirect(reverse('products:main_page'))


class DelFromCart(generic.View):
    def get(self, request, pk, amount):
        if request.user.is_authenticated:
            cart = Cart(request, user=request.user)
        else:
            cart = Cart(request)
        product = Products.objects.get(pk=pk)
        cart.del_item(product.id, amount=amount)
        return redirect(reverse('cart:cart_page'))


class ChangeAmount(generic.View):
    def post(self, request, pk):
        if request.user.is_authenticated:
            cart = Cart(request, user=request.user)
        else:
            cart = Cart(request)
        product = Products.objects.get(pk=pk)
        cart.add_or_update_item(product, request.POST['amount'])
        return redirect(reverse('cart:cart_page'))


class MakeOrder(generic.View):
    def post(self, request):
        user = request.user
        form = MakeOrders(request.POST)
        if user.is_authenticated:
            order = Order(request, user)
            pk = order.MakeOrder()
        else:
            if form.is_valid():
                order = Order(request, address=request.POST['address'], index=request.POST['index'])
                pk = order.MakeOrder()
        return redirect(reverse('cart:detail_order_page', args=[pk]))


class DetailOrder(generic.DetailView):
    template_name = 'cart/detail_order_page.html'
    model = Orders
    context_object_name = 'order'
