from django.urls import path
from .views import CartPage
from cart.views import AddToCart, DelFromCart, ChangeAmount, MakeOrder, DetailOrder
from django.conf import settings
from django.conf.urls.static import static

app_name = 'cart'

urlpatterns = [
    path('detail/', CartPage.as_view(), name='cart_page'),
    path('add_to_cart/<pk>/', AddToCart.as_view(), name='add_to_cart_page'),
    path('del_from_cart/<pk><int:amount>/', DelFromCart.as_view(), name='del_from_cart_page'),
    path('changeamount/<pk>/', ChangeAmount.as_view(), name='change_amount_page'),
    path('makeorder/', MakeOrder.as_view(), name='make_order_page'),
    path('order/<pk>/', DetailOrder.as_view(), name='detail_order_page'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)