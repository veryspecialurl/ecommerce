from django.db import models
from django.conf import settings


class Orders(models.Model):
    date_adding = models.DateField(auto_now_add=True)
    products = models.TextField()
    total_price = models.IntegerField()
    address = models.CharField(max_length=1024, null=True)
    index = models.IntegerField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, related_name='orders')

    def __str__(self):
        if self.user is None:
            return f'{self.date_adding} Анонимный заказ'
        else:
            return f'{self.date_adding} {self.user.email}'
