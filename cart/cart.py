from products.models import Products
from cart.models import Orders
from django.utils import timezone
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)


class Cart:

    @staticmethod
    def sum_dicts(dict1, dict2, compare_value=None):
        for elem in dict2:
            if elem not in dict1:
                dict1[elem] = dict2[elem]
            else:
                if compare_value is not None:
                    if dict2[elem][compare_value] > dict1[elem][compare_value]:
                        dict1[elem][compare_value] = dict2[elem][compare_value]
        return dict1

    def update_user_cart(self):
        self.user.cart = self.cart
        self.user.save(update_fields=['cart'])

    def __init__(self, request, user=None):
        self.session = request.session
        self.user = user
        cart = self.session.get('cart')
        if user == None:
            if not cart:
                cart = self.session['cart'] = {}
            self.cart = cart
        else:
            user_cart = eval(user.cart)
            if not cart:
                self.cart = self.session['cart'] = user_cart  # передалть добавить метод обновления
            else:
                self.cart = Cart.sum_dicts(cart, user_cart, 'amount')
                self.save()
                self.update_user_cart()

    def add_or_update_item(self, product, amount=1):
        product_id = str(product.id)
        if product_id not in self.cart:
            self.cart[product_id] = {'amount': amount, 'price': product.price}
        else:
            print(f'{product_id} already in cart')
            if amount == 1:
                self.cart[product_id]['amount'] += amount
            else:
                self.cart[product_id]['amount'] = int(amount)
        if self.user is not None:
            self.update_user_cart()
        self.save()

    def del_item(self, product_id, amount=0):
        if amount == 0:
            self.cart.pop(str(product_id))
            print(f'{product_id} deleted')
        else:
            self.cart[product_id]['amount'] -= amount
        self.save()
        if self.user != None:
            self.update_user_cart()

    def get_sum_price(self):
        return sum([product_id['amount'] * product_id['price'] for product_id in self.cart.values()])

    def clean(self):
        del self.session['cart']
        if self.user != None:
            self.user.cart = {}
            self.user.save(update_fields=['cart'])

    def len(self):
        return sum([self.cart[product_id]['amount'] for product_id in self.cart])

    def __iter__(self):
        products_id = self.cart.keys()
        products = Products.objects.filter(id__in=products_id)
        cart = self.cart.copy()
        for product in products:
            cart[str(product.id)]['product'] = product

        for product in cart.values():
            product['total_price'] = product['amount'] * product['price']
            yield product

    def save(self):
        self.session.modified = True

    def get_dict(self):
        return self.cart


class Order:

    def __init__(self, request, user=None, address=None, index=None):
        self.user = user
        self.cart = Cart(request, user)
        self.products = self.cart.get_dict()
        self.total_price = self.cart.get_sum_price()
        if self.user is not None:
            self.address = user.address
            self.index = user.index
        else:
            self.address = address
            self.index = index

    def MakeOrder(self):
        order = Orders.objects.create(products=self.products,
                                      total_price=self.total_price,
                                      address=self.address,
                                      user=self.user,
                                      index=self.index)
        self.cart.clean()
        return order.pk

    def DeleteOrder(self, id):
        Orders.objects.remove(id=id)
