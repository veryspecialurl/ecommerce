from django import forms
from users.models import MyUser


class MakeOrders(forms.ModelForm):
    class Meta:
        model = MyUser

        fields = [
            'address',
            'index'
        ]

        widgets = {
            'index': forms.NumberInput(
                attrs={'class': 'order-form', 'placeholder': 'Введите почтовый индекс...',
                       'max': '999999', 'min': '0', 'type': 'number'}),
            'address': forms.TextInput(
                attrs={'class': 'order-form', 'placeholder': 'Введите почтовый адресс...'}),
        }

    def clean(self):

        address = self.cleaned_data['address']
        if address.count(',') < 5:
            return self.add_error('address', 'Введите адресс в формате: Страна, Область, Город, Улица, Дом, Кв')
        else:
            for word in address.split(','):
                if len(word) < 2:
                    return self.add_error('address', 'Введите адресс в формате: Страна, Область, Город, Улица, Дом, Кв')
