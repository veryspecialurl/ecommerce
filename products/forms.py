from django import forms
from .models import Products


class AddToCart(forms.ModelForm):
    class Meta:
        model = Products

        fields = {}

class DelFromCart(forms.ModelForm):
    class Meta:
        model = Products

        fields = {}