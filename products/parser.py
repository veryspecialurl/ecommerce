"""
Citilink products parser
Парсит только одну группу, создавая главные группы и подгруппы
"""

import requests
import logging
import json
from bs4 import BeautifulSoup

from products.models import MainCategories, PrimaryCategories, Products

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def product_pars(url):
    products_dict = {}
    product_request = requests.get(url)
    product_html = BeautifulSoup(product_request.text, 'lxml')
    products = product_html.find_all('div', 'subcategory-product-item')
    for product in products:
        # Парсим по 1 стр. товаров из каждой подкатегории
        try:
            p_url = product.img['data-src']
        except AttributeError:
            p_url = product.img['src']
        except:
            p_url = None
        product_name = product.a['title']
        product_description = normalize_str(product.find('p', 'short_description').string)
        try:
            a = product.find('ins', 'subcategory-product-item__price-num').string.strip().split()
            product_price = int(''.join(a))
        except AttributeError:
            product_price = 0
        products_dict.update({product_name: {'cost': product_price, 'url': p_url,
                                             'description': product_description.strip()}})
        logging.info(f'Спарсили [{product_name}] цена [{product_price}]')
    return products_dict


def parser():
    """
    Парсер одного блока с citilink
    """
    logging.info('Start parsing')
    products_dict = {}
    url = 'https://www.citilink.ru/catalog/mobile/'
    request = requests.get(url)
    html_maingroups = BeautifulSoup(request.text, 'lxml')
    # Достаем список основных категорий
    uls = html_maingroups.find_all('ul', 'subnavigation__left_side_block_ul')
    for ul in uls:
        # Открываем каждую из категорий и смотрим есть ли в ней подкатегории
        logging.info(f'Парсим {ul.a.string}')
        main_category_name = ul.a.string
        products_dict.update({main_category_name: {}})
        new_request = requests.get(ul.a['href'])
        html_primary_groups_or_products = BeautifulSoup(new_request.text, 'lxml')
        new_uls = html_primary_groups_or_products.find_all('ul', 'subnavigation__left_side_block_ul')
        if new_uls:
            logging.info(f'Страница [{ul.a.string}] содержит подкатегории')
            for new_ul in new_uls:
                # Если содержит под категории перебираем уже их
                primary_category_name = new_ul.a.string.strip()
                products_dict[main_category_name].update({'have_primary': True})
                products_dict[main_category_name].update({primary_category_name: {}})
                logging.info(f'{primary_category_name}')
                products_dict[main_category_name][primary_category_name].update(product_pars(new_ul.a['href']))
        else:
            # Если не содержит под категории перебираем товары на стр
            logging.info(f'Страница [{ul.a.string}] не содержит подкатегории')
            products_dict[main_category_name].update({'have_primary': False})
            products_dict[main_category_name].update(product_pars(ul.a['href']))

    # Пакуем в JSON
    with open('products_parser.json', 'w') as file:
        json.dump(products_dict, file)
    logging.info('Finished parsing')


def normalize_str(str):
    return ' '.join(str.strip().replace('\n', '').split())


# import products.parser as par

def MakeProducts(filename='products_parser.json'):
    with open(filename, 'r') as file:
        categories = json.load(file)
        for category in categories:
            if categories[category]['have_primary']:
                try:
                    MainCategories.objects.get(name=category)
                except:
                    main_category = MainCategories.objects.create(name=category, have_primary=True)
                    logging.info(f'MainCategory:{category} created')
                    categories[category].pop('have_primary')
                    for primary_group in categories[category]:
                        make_products(True, primary_group, main_category, categories[category][primary_group])
            else:
                make_products(False, category, None, categories[category])
    print('Done!')


def make_products(is_primary, group_name, main_cat, dict):
    if not is_primary:
        dict.pop('have_primary')
        try:
            MainCategories.objects.get(name=group_name)
        except:
            main_category = MainCategories.objects.create(name=group_name)
            logging.info(f'MainCategory:{group_name} created')
            for product in dict:
                try:
                    Products.objects.get(name=product)
                except Products.DoesNotExist:
                    if dict[product]['url'] != None:
                        Products.objects.create(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=dict[product]['url'],
                            description=dict[product]['description'],
                            main_category=main_category
                        )
                    else:
                        Products.objects.create(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=None,
                            description=dict[product]['description'],
                            main_category=main_category
                        )
                    logging.info(f'Product:{product} created')
    else:
        try:
            PrimaryCategories.objects.get(name=group_name)
        except:
            primary_category = PrimaryCategories.objects.create(name=group_name, main_group=main_cat)
            logging.info(f'PrimaryCategory:{group_name} created')
            for product in dict:
                try:
                    Products.objects.get(name=product)
                except Products.DoesNotExist:
                    if dict[product]['url'] != None:
                        Products.objects.create(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=dict[product]['url'],
                            description=dict[product]['description'],
                            primary_category=primary_category
                        )
                    else:
                        Products.objects.create(
                            name=product,
                            price=dict[product]['cost'],
                            image_url=None,
                            description=dict[product]['description'],
                            primary_category=primary_category
                        )
                    logging.info(f'Product:{product} created')
