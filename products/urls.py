from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from .views import *

app_name = 'products'

urlpatterns = [
    path('', MainPage.as_view(), name='main_page'),
    path('<slug>/', MainGroup.as_view(), name='main_group_page'),
    path('p/<slug>/', PrimaryGroup.as_view(), name='primary_group_page'),
    path('product/<pk>/', ProductPage.as_view(), name='detail_product_page'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
