from django.shortcuts import render, redirect
from django.views import generic
import logging
from .models import *
from cart.cart import *
from users.forms import AuthForm
from .forms import AddToCart


class MainPage(generic.View):
    def get(self, request):
        context = {
            'main_categories': MainCategories.objects.all(),
            'cart': Cart(request),
            'auth_form': AuthForm(),
            'new_products': Products.objects.order_by('date_adding')[:18]
        }
        return render(request, 'products/main_page.html', context)


class MainGroup(generic.View):
    def get(self, request, slug):
        try:
            main_group = MainCategories.objects.get(slug=slug)
        except MainCategories.DoesNotExist:
            logging.info(slug)
        main_context = {
            'cart': Cart(request),
            'auth_form': AuthForm(),
        }
        if main_group.have_primary:
            context = {
                'primary_categories': main_group.primary_group.all(),
                'new_products': Products.objects.filter(primary_category__in=main_group.primary_group.all()).order_by('date_adding')[:12]
            }
            main_context.update(context)
            return render(request, 'products/primary_page.html', main_context)
        else:
            context = {
                'products': Products.objects.filter(main_category=main_group),
                'cart_form': AddToCart(),
            }
            main_context.update(context)
            return render(request, 'products/products.html', main_context)


class PrimaryGroup(generic.View):
    def get(self, request, slug):
        primary_group = PrimaryCategories.objects.get(slug=slug)
        main_context = {
            'cart': Cart(request),
            'auth_form': AuthForm(),
            'products': primary_group.products.exclude(image_url=None),
        }
        return render(request, 'products/products.html', main_context)


class ProductPage(generic.DetailView):
    model = Products
    template_name = 'products/detail_product.html'
    context_object_name = 'product'
