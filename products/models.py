from django.db import models
from django.utils.text import slugify
from django.shortcuts import reverse
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class MainCategories(models.Model):
    name = models.CharField(max_length=1024, unique=True)
    date_making = models.DateField(auto_now_add=True)
    slug = models.SlugField(blank=True, max_length=1024)
    have_primary = models.BooleanField(default=False)

    REQUIRED_FIELDS = [
        'name',
    ]

    class Meta:
        indexes = [
            models.Index(fields=['name'])
        ]

    def get_absolute_url(self):
        return reverse('products:main_group_page', args=[self.slug])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        # logging.info(f'Created MAIN CATEGORY: [{self.name}]')
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class PrimaryCategories(models.Model):
    name = models.CharField(max_length=1024, unique=True)
    date_making = models.DateField(auto_now_add=True)
    slug = models.SlugField(blank=True, max_length=1024)
    main_group = models.ForeignKey('MainCategories', null=True, on_delete=models.CASCADE, related_name='primary_group')

    REQUIRED_FIELDS = [
        'name',
    ]

    class Meta:
        indexes = [
            models.Index(fields=['name'])
        ]

    def get_absolute_url(self):
        return reverse('products:primary_group_page', args=[self.slug])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        # logging.info(f'Created PRIMARY CATEGORY : [{self.name}]')
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Products(models.Model):
    main_category = models.ForeignKey('MainCategories', on_delete=models.CASCADE, related_name='products', null=True)
    primary_category = models.ForeignKey('PrimaryCategories', on_delete=models.CASCADE, related_name='products',
                                         null=True)
    name = models.CharField(max_length=1024, unique=True)
    price = models.IntegerField()
    description = models.TextField()
    main_characteristic = models.TextField()
    image = ProcessedImageField(upload_to='products_images/',
                                format='PNG',
                                options={'quality': 100},
                                default='products_images/no_image.png')
    slug = models.SlugField(blank=True, max_length=1024)
    image_url = models.TextField(null=True)
    date_adding = models.DateField(auto_now_add=True)

    REQUIRED_FIELDS = [
        'name',
        'price',
        'description',
    ]

    class Meta:
        indexes = [
            models.Index(fields=['name'])
        ]

    def get_absolute_url(self):
        return reverse('products:detail_product_page', args=[self.pk])

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name, allow_unicode=True)
        # logging.info(f'Created PRIMARY CATEGORY PRODUCT: [{self.name}]')
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.name
