from django.contrib import admin
from .models import *

admin.site.register(MainCategories)
admin.site.register(PrimaryCategories)
admin.site.register(Products)