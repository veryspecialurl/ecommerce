from django.views.generic import View, FormView, TemplateView
from .models import MyUser
from .forms import AuthForm, RegisterForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, reverse, render
from django.urls import reverse_lazy
from django.contrib import messages
from cart.cart import Cart
from django.conf import settings
import requests
from .managers import Vk


class Login(View):
    def post(self, request):
        user = authenticate(username=request.POST['email'], password=request.POST['password'])
        if user is not None:
            login(request, user)
            Cart(request, user)
            return redirect(reverse('products:main_page'))
        else:
            messages.error(request, 'Неправильный логин или пароль')
            return redirect(reverse('products:main_page'))


class Logout(View):
    def get(self, request):
        logout(request)
        return redirect(reverse('products:main_page'))


class Register(FormView):
    form_class = RegisterForm
    template_name = 'users/register_page.html'
    success_url = reverse_lazy('products:main_page')

    def form_valid(self, form):
        MyUser.objects.create_user(email=form.cleaned_data['email'],
                                   password=form.cleaned_data['password'],
                                   address=form.cleaned_data['address'],
                                   index=form.cleaned_data['index'])
        user = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password'])
        login(self.request, user)
        redirect(self.get_success_url())
        return super().form_valid(form)


class Vklogin(View):
    def get(self, request):
        client_id = settings.APP_CODE
        redirect_uri = request.build_absolute_uri(reverse('users:success_page_auth'))
        url = f'https://oauth.vk.com/authorize?client_id={client_id}&scope=email&display=popup&redirect_uri={redirect_uri}&response_type=code&v=5.103'
        return redirect(url)


class VkloginSuccess(View):
    def get(self, request):
        redirect_uri = request.build_absolute_uri(reverse('users:success_page_auth'))
        accept_key_request = requests.get('https://oauth.vk.com/access_token', params={'client_id': settings.APP_CODE,
                                                                                       'client_secret': 'DKJxeMTScKjtwputpLkM',
                                                                                       'redirect_uri': redirect_uri,
                                                                                       'code': request.GET['code']})
        request.session['access_token'] = accept_key_request.json()['access_token']
        request.session['user_id'] = accept_key_request.json()['user_id']
        request.session.modified = True
        try:
            user = MyUser.objects.get(vk_user_id=request.session['user_id'])
        except MyUser.DoesNotExist:
            return redirect('users:vk_register_page')
        user = authenticate(vk_user_id=request.session['user_id'])
        login(request, user, backend='users.models.VkAuthenticate')
        Cart(request, user)
        return redirect(reverse('products:main_page'))


class VkloginRegister(View):
    def get(self, request):
        vk_authenticated = Vk(request.session['access_token'])
        data_dir = vk_authenticated.main_data(request.session['user_id'])
        request.session['first_name'] = data_dir['first_name']
        request.session['last_name'] = data_dir['last_name']
        request.session['photo'] = data_dir['photo']
        request.session.modified = True
        context = {}
        context['first_name'] = request.session['first_name']
        context['last_name'] = request.session['last_name']
        context['photo'] = request.session['photo']
        context['form'] = RegisterForm()
        return render(request, 'users/vk_register_page.html', context)

    def post(self, request):
        form = RegisterForm(request.POST)
        if form.is_valid():
            MyUser.objects.create_user(email=form.cleaned_data['email'],
                                       password=form.cleaned_data['password'],
                                       address=form.cleaned_data['address'],
                                       index=form.cleaned_data['index'],
                                       first_name=request.session['first_name'],
                                       second_name=request.session['last_name'],
                                       photo_url=request.session['photo'],
                                       vk_user_id=request.session['user_id'])
            user = authenticate(email=form.cleaned_data['email'], password=form.cleaned_data['password'])
            del request.session['first_name']
            del request.session['last_name']
            del request.session['photo']
            login(request, user)
            return redirect(reverse('products:main_page'))


class AuthWithVk(View):
    def get(self, request):
        context = {}
        context['first_name'] = request.session['first_name']
        context['last_name'] = request.session['last_name']
        context['photo'] = request.session['photo']
        context['form'] = AuthForm()
        return render(request, 'users/vk_auth_page.html', context)

    def post(self, request):
        user = authenticate(email=request.POST['email'], password=request.POST['password'])
        if user is None:
            messages.error(request, 'Неправильный логин или пароль')
            return redirect(reverse('users:vk_login_page'))
        user.vk_user_id = request.session['user_id']
        user.first_name = request.session['first_name']
        user.second_name = request.session['last_name']
        user.photo_url = request.session['photo']
        user.save(update_fields=['vk_user_id', 'first_name', 'second_name', 'photo_url'])
        login(request, user)
        Cart(request, user)
        return redirect(reverse('products:main_page'))


class UserPage(TemplateView):
    template_name = 'users/user_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cart'] = Cart(self.request)
        return context
