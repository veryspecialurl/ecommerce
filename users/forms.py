from django import forms
from .models import MyUser


class AuthForm(forms.ModelForm):
    class Meta:
        model = MyUser

        fields = {
            'email',
            'password'
        }

        widgets = {
            'password': forms.PasswordInput(
                attrs={'class': 'auth-form', 'id': 'InputPassword', 'placeholder': 'Введите пароль...'}),
            'email': forms.EmailInput(
                attrs={'class': 'auth-form', 'id': 'InputEmail', 'placeholder': 'Введите Email...'}),
        }


class RegisterForm(forms.ModelForm):
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'auth-form', 'id': 'InputPassword', 'placeholder': 'Повторте пароль...'}), )

    class Meta:
        model = MyUser

        fields = {
            'email',
            'password',
            'address',
            'index'
        }

        required_fields = {
            'email',
            'password',
            'address',
            'index'
        }

        widgets = {
            'password': forms.PasswordInput(
                attrs={'class': 'auth-form', 'id': 'InputPassword', 'placeholder': 'Введите пароль...'}),
            'email': forms.EmailInput(
                attrs={'class': 'auth-form', 'id': 'InputEmail', 'placeholder': 'Введите Email...'}),
            'address': forms.TextInput(
                attrs={'class': 'auth-form', 'id': 'InputEmail', 'placeholder': 'Введите ваш почтовый адресс...'}),
            'index': forms.NumberInput(
                attrs={'class': 'auth-form', 'placeholder': 'Введите почтовый индекс...', 'min': '0',
                       'type': 'number'}),
        }

    def clean(self):

        address = self.cleaned_data['address']

        if self.cleaned_data.get('password') != self.cleaned_data.get('password2'):
            return self.add_error('password', 'Пароли должны совпадать')

        if address.count(',') < 5:
            return self.add_error('address', 'Введите адресс в формате: Страна, Область, Город, Улица, Дом, Кв')
        else:
            for word in address.split(','):
                if len(word) < 2:
                    return self.add_error('address',
                                          'Введите адресс в формате: Страна, Область, Город, Улица, Дом, Кв')
