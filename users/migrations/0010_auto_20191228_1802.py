# Generated by Django 3.0 on 2019-12-28 15:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0009_myuser_index'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myuser',
            name='address',
            field=models.CharField(default=2, max_length=1024),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='myuser',
            name='index',
            field=models.CharField(max_length=1024),
        ),
    ]
