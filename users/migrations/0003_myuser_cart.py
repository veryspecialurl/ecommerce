# Generated by Django 3.0 on 2019-12-23 16:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20191218_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='cart',
            field=models.TextField(null=True),
        ),
    ]
