from django.urls import path
from .views import Login, Logout, Register, Vklogin, VkloginSuccess, VkloginRegister, AuthWithVk, UserPage

app_name = 'users'

urlpatterns = [
    path('login/', Login.as_view(), name='login_page'),
    path('logout/', Logout.as_view(), name='logout_page'),
    path('register/', Register.as_view(), name='register_page'),
    path('vkauth/', Vklogin.as_view(), name='vkauth_page'),
    path('vksuccessauth', VkloginSuccess.as_view(), name='success_page_auth'),
    path('registervk/', VkloginRegister.as_view(), name='vk_register_page'),
    path('vklogin/', AuthWithVk.as_view(), name='vk_login_page'),
    path('detail/', UserPage.as_view(), name='user_page'),
]