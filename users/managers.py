from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.hashers import make_password
import requests


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password, **kwargs):
        if not email:
            raise ValueError('Не был введен email')
        if not password:
            raise ValueError('Не был введен пароль')
        user = self.model(email=self.normalize_email(email), **kwargs)
        user.password = make_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **kwargs):
        kwargs.setdefault('is_staff', True)
        return self.create_user(email, password, **kwargs)


class Vk():

    def __init__(self, access_token):
        self.access_token = access_token

    def main_data(self, user_id):
        data = requests.get(
            f'https://api.vk.com/method/users.get?user_ids={user_id}&fields=photo&access_token={self.access_token}&v=5.103').json()['response'][0]
        return {'first_name': data['first_name'],
                'last_name': data['last_name'],
                'photo': data['photo']}
