from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.hashers import check_password, make_password
from .managers import CustomUserManager


class MyUser(AbstractBaseUser):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=150)
    cart = models.TextField(default='{}')
    is_staff = models.BooleanField(default=False)
    address = models.CharField(max_length=1024)
    index = models.CharField(max_length=1024)
    vk_user_id = models.CharField(max_length=25, unique=True, null=True)
    first_name = models.CharField(max_length=100, null=True)
    second_name = models.CharField(max_length=100, null=True)
    photo_url = models.URLField(null=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = [
        'password',
        'address'
    ]

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True


class CustomAuthenticate:

    def authenticate(self, request, email, password=None):
        try:
            user = MyUser.objects.get(email=email)
            if check_password(password, user.password):
                return user
            else:
                return None
        except MyUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return MyUser.objects.get(id=user_id)
        except:
            return None


class VkAuthenticate:

    def authenticate(self, request, vk_user_id):
        try:
            user = MyUser.objects.get(vk_user_id=vk_user_id)
            return user
        except MyUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return MyUser.objects.get(id=user_id)
        except:
            return None
